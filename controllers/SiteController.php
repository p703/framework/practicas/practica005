<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Numeros;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionSumar()
    {
        $model=new Numeros();
        $model->tipo="sumar";        
        
        $datos=Yii::$app->request->post(); // recoger lo que me manda el formulario
        
        // compruebo si me llega algo del formulario y cumple las rules
        if($model->load($datos) && $model->validate()){ 
            // realizo la suma
            $resultado=$model->operacion();
            //mando la suma a la vista
            return $this->render("resultado",[
                "resultado" => $resultado,
                 "operacion" => "suma"
            ]);
        }
        
        // si no ha entrado en el if anterior cargo el formulario vacio o con errores
        return $this->render('numeros',[
            "model" => $model,
            "operacion" => "Sumar"
        ]);
        
        
    }
    
    public function actionRestar()
    {
         $model=new Numeros();
         $model->tipo="restar";        
        
        $datos=Yii::$app->request->post(); // recoger lo que me manda el formulario
        
        // compruebo si me llega algo del formulario y cumple las rules
        if($model->load($datos) && $model->validate()){ 
            // realizo la suma
            $resultado=$model->operacion();
            //mando la suma a la vista
            return $this->render("resultado",[
                "resultado" => $resultado,
                 "operacion" => "resta"
            ]);
        }
        
        // si no ha entrado en el if anterior cargo el formulario vacio o con errores
        return $this->render('numeros',[
            "model" => $model,
            "operacion" => "Restar"
        ]);
    }
    
    public function actionMultiplicar()
    {
          $model=new Numeros();
          $model->tipo="multiplicar";        
        
        $datos=Yii::$app->request->post(); // recoger lo que me manda el formulario
        
        // compruebo si me llega algo del formulario y cumple las rules
        if($model->load($datos) && $model->validate()){ 
            // realizo la suma
            $resultado=$model->operacion();
            //mando la suma a la vista
            return $this->render("resultado",[
                "resultado" => $resultado,
                "operacion" => "multiplicacion"
            ]);
        }
        
        // si no ha entrado en el if anterior cargo el formulario vacio o con errores
        return $this->render('numeros',[
            "model" => $model,
            "operacion" => "Multiplicar"
        ]);
    }
    
    public function actionDividir()
    {
        $model=new Numeros();
        $model->tipo="dividir";        
        
        $datos=Yii::$app->request->post(); // recoger lo que me manda el formulario
        
        // compruebo si me llega algo del formulario y cumple las rules
        if($model->load($datos) && $model->validate()){ 
            // realizo la suma
            $resultado=$model->operacion();
            //mando la suma a la vista
            return $this->render("resultado",[
                "resultado" => $resultado,
                "operacion" => "division"
            ]);
        }
        
        // si no ha entrado en el if anterior cargo el formulario vacio o con errores
        return $this->render('numeros',[
            "model" => $model,
            "operacion" => "Dividir"
        ]);
    }
    
    
    public function actionOperar(){
        $model=new Numeros();
        $model->tipo="todo";        
        
        $datos=Yii::$app->request->post(); // recoger lo que me manda el formulario
        
        // compruebo si me llega algo del formulario y cumple las rules
        if($model->load($datos) && $model->validate()){ 
            // realizo todas las operaciones
            $model->operarTodo();
            //mando la suma a la vista
            return $this->render("mostrarTodo",[
                "model" => $model
            ]);
        }
        
        // si no ha entrado en el if anterior cargo el formulario vacio o con errores
        return $this->render('numeros',[
            "model" => $model,
            "operacion" => "Calcular"
        ]);
    }
   
}
